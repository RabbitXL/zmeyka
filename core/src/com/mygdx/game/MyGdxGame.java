package com.mygdx.game;


import com.badlogic.gdx.Game;

import com.mygdx.game.view.MenuScreen;


public class MyGdxGame extends Game {


	private MenuScreen menuScreen;

	@Override
	public void create() {

		menuScreen = new MenuScreen(this);
		setScreen(menuScreen);
	}
}
