package com.mygdx.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.model.Hero;
import com.mygdx.game.view.GameScreen;

public class HeroController {
	Hero hero;

	public HeroController(Hero hero) {
		this.hero = hero;
	}

	public void handle (){
		float speed = 10;
		float to=0;
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
			to =90  *GameScreen.deltaCff  ;
		}
		else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
			to =- 90  * GameScreen.deltaCff ;
		}
		hero.setRotat( (hero.getRotat()  + to));
		//System.out.println(hero.getRotat());
		hero.setPosition(hero.getX()-MathUtils.sinDeg(hero.getRotat())*speed*GameScreen.deltaCff,
				hero.getY() + MathUtils.cosDeg(hero.getRotat())*speed*GameScreen.deltaCff);
	}
}
