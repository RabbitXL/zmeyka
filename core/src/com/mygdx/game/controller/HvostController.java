package com.mygdx.game.controller;

import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.model.Hero;
import com.mygdx.game.model.Hvost;
import com.mygdx.game.utils.Constants;
import com.mygdx.game.view.GameScreen;

public class HvostController {
	Hvost hv;
	Hero hero;
	double x1;
	double y1;
	double x2;
	double y2;
	double distance;
	float speed;
	double rot;
	public HvostController(Hvost hv, Hero hero) {
		this.hv = hv;
		this.hero = hero;
	}

	public void go() {
		x1 = hv.getX() + Constants.hvostSize / 2;
		y1 = hv.getY() + Constants.hvostSize / 2;
		x2 = hero.getX() + Constants.headSize / 2;
		y2 = hero.getY() + Constants.headSize / 2;
		distance = Math.hypot(x1-x2,y1-y2);
		speed = (float) distance/0.3f;
		rot = getRot(x1, y1,
				x2, y2);
		hv.setRotat(rot);
		//System.out.println(rot);
		hv.setPosition(hv.getX() + MathUtils.sinDeg(hv.getRotat()) * speed * GameScreen.deltaCff,
				hv.getY() + MathUtils.cosDeg(hv.getRotat()) * speed * GameScreen.deltaCff);
	}

	private double getRot(double x1, double y1, double x2, double y2) {
		double result;
		result = (Math.atan2((double) (x2 - x1), (double) (y2 - y1)) / Math.PI * 180);
		//result = (result < 0 ) ? result+360 : result;
		//result -=180;
		return result;
	}

}
