package com.mygdx.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;


public abstract  class CircleGameObject {
	public float getRotat() {
		return rotat;
	}

	public void setRotat(double rotat) {
		this.rotat = (float )rotat;
	}

	float rotat = 0;
	public Polygon getBounds() {
		return bounds;
	}

	Polygon bounds;
	Sprite object;

	public CircleGameObject(Texture texture, float x, float y, float w) {
		bounds = new Polygon(new float[]{x,y, w, y, w, w, x,w });
		bounds.setOrigin(w/2,w/2);
		bounds.setPosition(x,y);
		object = new Sprite(texture);
		object.setSize(w,w);
		object.setPosition(x, y);
		object.setOrigin(w/2,w/2);

	}

	public void draw (SpriteBatch batch){
		object.setRotation(rotat);
		object.setPosition(bounds.getX(), bounds.getY());
		object.draw(batch);
	}

	public void setPosition(float x, float y){
		object.setPosition(x, y);
		bounds.setPosition(x, y);
	}
	public float getX(){
		return bounds.getX();
	}
	public float getY(){
		return bounds.getY();
	}
}
