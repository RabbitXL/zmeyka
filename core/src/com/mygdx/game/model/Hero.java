package com.mygdx.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.controller.HeroController;

public class Hero extends CircleGameObject {

	HeroController controller;

	public float getPrevPosX() {
		return prevPosX;
	}

	public float getPrevPosY() {
		return prevPosY;
	}

	float prevPosX, prevPosY;

	@Override
	public void setPosition(float x, float y) {
		prevPosX = getX();
		prevPosY = getY();
		super.setPosition(x, y);
	}

	public Hero(Texture textureHero, float x, float y, float radius) {
		super(textureHero, x, y, radius);
		controller = new HeroController(this);
		//this.setRotat(180);


	}

	@Override
	public void draw(SpriteBatch batch) {
		super.draw(batch);
		controller.handle();
	}







}
