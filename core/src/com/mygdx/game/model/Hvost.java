package com.mygdx.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.controller.HvostController;

public class Hvost extends CircleGameObject {
	HvostController hvc;

	@Override
	public void draw(SpriteBatch batch) {
		super.draw(batch);
		hvc.go();
	}

	public Hvost(Hero hero, Texture texture, float x, float y, float diametr) {
		super(texture, x, y, diametr);
		hvc = new HvostController(this,hero);
	}
}
