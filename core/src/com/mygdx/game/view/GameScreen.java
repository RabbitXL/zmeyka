package com.mygdx.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.model.Eda;
import com.mygdx.game.model.Hero;
import com.mygdx.game.model.Hvost;
import com.mygdx.game.utils.Constants;

public class GameScreen implements Screen {
	Texture heroTexture;
	SpriteBatch batch;
	Eda eda;
	int countEda =1;
	private Hero hero;
	Hvost hvost;
	float aspectRatio;
	private OrthographicCamera camera;
	public static float deltaCff;
	private Texture edaTexture;
	private Texture hvostTexture;


	@Override
	public void show() {
		batch = new SpriteBatch();
		heroTexture = new Texture(Gdx.files.internal("open.png"));
		hvostTexture = new Texture(Gdx.files.internal("close.png"));
		hero = new Hero(heroTexture,  50,50,Constants.headSize);
		hvost = new Hvost(hero, hvostTexture,40,40,Constants.hvostSize);
		edaTexture = new Texture(Gdx.files.internal("cell.png"));
		eda = new Eda(edaTexture,2,2,1);


	}

	@Override
	public void render(float delta) {

	Gdx.gl.glClearColor(0,0,0,1);
	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

	camera.update();
	batch.setProjectionMatrix(camera.combined);
	deltaCff = delta;
	batch.begin();
	if (countEda == 0){
		countEda = 1;
		eda = new Eda(edaTexture,MathUtils.random(0,Constants.windowWidth),MathUtils.random(0,Constants.windowHeight),10);
	}
	eda.draw(batch);
	//hero.setPosition(hero.getBounds().x+delta*10,hero.getBounds().y+delta*10);
	hero.draw(batch);

	hvost.draw(batch);
	batch.end();

	}

	@Override
	public void resize(int width, int height) {
		aspectRatio =(float)Gdx.graphics.getWidth()/Gdx.graphics.getHeight();
		camera = new OrthographicCamera(Constants.windowWidth, Constants.windowWidth/aspectRatio);
		camera.position.set(new Vector3(Constants.windowWidth/2,Constants.windowWidth/aspectRatio/2,0));
		camera.update();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		batch.dispose();
		heroTexture.dispose();
	}
}
